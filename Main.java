//it inherits the package as it is protected modifier

import Protected.Product;

//It inherits the class product as it is from protected modifier 

public class Main extends Product {
	public static void main(String[] args) {

		// creating object of the class account

		Account acc = new Account();
		acc.setAcc_no(1333715286566L);
		acc.setName("Ziya");
		acc.setBalance(1345672);
		System.out.println("This is the output Based on Private and Encapsulation..");
		System.out.println("-------------------------------------------------------");
		System.out.println("Account number :" + acc.getAcc_no());
		System.out.println("Name           :" + acc.getBalance());
		System.out.println("Balance        :" + acc.getBalance());
		System.out.println("-------------------------------------");
		System.out.println("\n");

		// creating the object of the sub class Main
		/**
		 * here we must create the object for Subclass as it is protected modifier
		 */

		Main en = new Main();
		System.out.println("This is the output Based on Protected");
		System.out.println("--------------------------------------");
		System.out.println("Product ID    :" + en.productId);
		System.out.println("Product Name  :" + en.productname);
		System.out.println("Product Price :" + en.productprice);
		System.out.println("-----------------------------------");
		System.out.println("\n");

		// creating the object of the sub class Status
		/**
		 * here we must create the object for Subclass as it is abstract modifier
		 */

		Status st = new Status();
		st.placeOrder();
		st.cancelOrder();
		st.traceOrder();
	}
}
