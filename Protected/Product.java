package Protected;

/**
 * This is the implementation of the class Product
 * here we are implementing using 
 * the Protected access Modifier
 * @author tashaik
 */

public class Product {
	protected int productId = 100;
	protected String productname = "watch" ;
	protected int productprice = 2588;

}
