package Default;
/**
 * This is the implementation of the class Employee
 * here we are implementing using 
 * the Default access Modifier
 * @author tashaik
 */

 public class Employee {
	int empid;
	String empname;
	String Designation;
	int salary;
	
	/**
	 * created a Employee class constructor and
	 * calling  the arguments
	 * @param empid
	 * @param empname
	 * @param designation
	 * @param salary 
	 */

	public Employee(int empid, String empname, String designation, int salary) {
		this.empid = empid;
		this.empname = empname;
		this.Designation = designation;
		this.salary = salary;
	}

	/**
	 * override the default toString method of Employee so that Employee info can be
	 * printed in human readable format
	 */

	@Override
	public String toString() {
		String Employeeinfo = "";
		Employeeinfo += "Employee Id      :     " + this.empid + "\n";
		Employeeinfo += "Employee Name    :     " + this.empname + "\n";
		Employeeinfo += "Designation      :     " + this.Designation + "\n";
		Employeeinfo += "Salary           :     " + this.salary + "\n";
		return Employeeinfo;
	}

	public static void main(String[] args) {
		
		// creating the object of the class Employee

		Employee emp = new Employee(1,"Ziya","Software Engineer",374856);
		System.out.println("This is the output based on Default");
		System.out.println("/n");
		System.out.println(emp);
	}
}
