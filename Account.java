/**
 * This is the implementation of the class Account here we are implementing
 * using the Private access Modifier 
 * @author tashaik
 */
public class Account {

	/*
	 * we should acces the variables as private as we are implementing Private
	 * modifier
	 */

	private long acc_no;
	private String name;
	private int balance;

	/*
	 * creating setters and setter methods as the data members are private
	 */

	public long getAcc_no() {
		return acc_no;
	}

	public void setAcc_no(long acc_no) {
		this.acc_no = acc_no;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}
}
