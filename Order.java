/**
 * This is the implementation of the class Order
 * here we are implementing using 
 * the abstract access Modifier
 * @author tashaik
 */
interface  Order {
	//creating the methods with no body
	public void placeOrder();
	public void cancelOrder();
	public void traceOrder();
}

//implementation is provided by others i.e. unknown by end user
 class Status implements  Order {
	 public void placeOrder() {
		System.out.println("Placement Status : order placed");
	 }
		public void cancelOrder() {
			System.out.println("Cancellation status : order cancelled");
		}                                                                                                                          
		public void traceOrder() {
			System.out.println("Tracking Status : order traced");
		}
	
	
}
